# Les fondations

Difficile de parler de developpement web sans aborder les fondements de cette technologie.

::: info DISCLAIMER
Une grande partie du text qui suit est éctrait de la fantastique documentation fournie par Mozilla.
https://developer.mozilla.org/fr/docs/Web/HTTP
:::

## HTTP

**Hypertext Transfer Protocol (HTTP) (ou protocole de transfert hypertexte en français)** est un protocole de la [couche application](https://fr.wikipedia.org/wiki/Couche_application) servant à transmettre des documents hypermédias, comme HTML. Il a été conçu la communication entre les navigateurs web et les serveurs web mais peut également être utilisé à d'autres fins. Il suit le modèle classique [client-serveur](https://fr.wikipedia.org/wiki/Client-serveur) : un client ouvre une connexion, effectue une requête et attend jusqu'à recevoir une réponse. Il s'agit aussi d'un [protocole sans état](https://fr.wikipedia.org/wiki/Protocole_sans_%C3%A9tat), ce qui signifie que le serveur ne conserve aucune donnée (on parle d'état) entre deux requêtes.

### Messages

Un message (ou paquet) HTTP constitue le format d'échange entre un client et un serveur. Il exitste deux types de messages HTTP : des **requêtes** et des **réponses**.

Les differents type de messages HTTP partagent une structure commune.

- une première ligne (start line) qui définit le type de message
- un ensemble d'en têtes (headers) qui définissent des informations supplémentaires
- une ligne vide
- un corps (body) qui contient les données

![Structure des messages HTTP](./httpmsgstructure2.png)_source de l'image: [MDN Web Docs](https://developer.mozilla.org/en-US/docs/Web/HTTP/Messages)_

:::info 🔧 OUTILS

- httpbin.org pour nos tests. Un outils très chouette pour comprendre le protocol. Ce sera notre **serveur**.
- [curl](https://curl.haxx.se/) un outils en ligne de commande (CLI) qui permet de faire des requêtes HTTP. Il est installer par défaut sur la plupart des système d'exploitation. (même Windows oui oui 🤯)
  :::

### Requêtes

HTTP définit un ensemble de **méthodes de requête** qui indiquent l'action que l'on souhaite réaliser sur la ressource indiquée. Ces méthodes sont souvent appelées verbes HTTP. Chacun d'eux implémente une sémantique différente.

Faisons une requête _GET_ (par défaut pour curl) en affichant plus d'infos (`-v`) et étudions le résultat.

```bash
curl -v https://httpbin.org/get
```

```
...
> GET /get HTTP/2
> Host: httpbin.org
> user-agent: curl/7.77.0
> accept: */*
>
...
```

Nous avons donc:

- notre **start line** qui se découpent en trois parties :
  - le **methode** ici: GET
  - l'**URL** ici /get
  - le **version du protocol** (HTTP/2)
- une **liste d'en têtes** (headers) sous la forme de couples clef valeurs séparées par un `:`
- une **ligne vide**
- le **corps** qui est vide ici

### Réponses

Ok mais alors c'est comment une réponse ? 🧐
Faisons donc une nouvelle requête _GET_ en affichant les en-têtes renvoyées par le serveur (`-i`) et regardons le résultat.

```bash
curl -i https://httpbin.org/get
```

```
HTTP/2 200
date: Fri, 03 Dec 2021 14:51:31 GMT
content-type: application/json
content-length: 256
server: gunicorn/19.9.0
access-control-allow-origin: *
access-control-allow-credentials: true

{
  "args": {},
  "headers": {
    "Accept": "*/*",
    "Host": "httpbin.org",
    "User-Agent": "curl/7.77.0",
    "X-Amzn-Trace-Id": "Root=1-61aa2ef3-66caae8446a1f4967614a3f0"
  },
  "origin": "81.250.131.200",
  "url": "https://httpbin.org/get"
}
```

- notre **start line** qui se découpent en deux parties :
  - le **version du protocol** (HTTP/2)
  - le **status code** ici: 200
- une **liste d'en têtes** (headers) sous la forme de couples clef valeurs séparées par un `:`
- une **ligne vide**
- le **corps** ici la réponse du serveurs

::: info 📝 EXERCICES

- Quelle en-tête est en rapport avec le résultat de la commande `curl -s https://httpbin.org/get | wc -c`.
- Se renseigner sur le status code [418](https://developer.mozilla.org/fr/docs/Web/HTTP/Status/418)
:::

### Méthodes

Voici les différentes méthodes HTTP disponibles.

#### GET

La méthode GET demande une représentation de la ressource spécifiée. Les requêtes GET doivent uniquement être utilisées afin de récupérer des données.

#### HEAD

La méthode HEAD demande une réponse identique à une requête GET pour laquelle on aura omis le corps de la réponse (on a uniquement l'en-tête).

```bash
curl --head https://httpbin.org/get
```

```
HTTP/2 200 
date: Fri, 03 Dec 2021 15:17:11 GMT
content-type: application/json
content-length: 256
server: gunicorn/19.9.0
access-control-allow-origin: *
access-control-allow-credentials: true
```

#### POST

La méthode POST est utilisée pour envoyer une entité vers la ressource indiquée. Cela entraîne généralement un changement d'état ou des effets de bord sur le serveur.

```bash
curl -X POST -H "Content-Type: application/x-www-form-urlencoded" -d "param1=value1&param2=value2" https://httpbin.org/post
```

```
{
  "args": {}, 
  "data": "", 
  "files": {}, 
  "form": {
    "param1": "value1", 
    "param2": "value2"
  }, 
  "headers": {
    "Accept": "*/*", 
    "Content-Length": "27", 
    "Content-Type": "application/x-www-form-urlencoded", 
    "Host": "httpbin.org", 
    "User-Agent": "curl/7.77.0", 
    "X-Amzn-Trace-Id": "Root=1-61aa3572-3af4fc955afb15dd0850808e"
  }, 
  "json": null, 
  "origin": "81.250.131.200", 
  "url": "https://httpbin.org/post"
}
```

::: info 📝 EXERCICE
Utiliser la commande `curl -X POST -H "Content-Type: application/json" -d '{"key1":"value1", "key2":"value2"}' https://httpbin.org/post` et étudier le résultat.
:::

#### PUT

La méthode PUT remplace toutes les représentations actuelles de la ressource visée par le contenu de la requête.

```bash
curl -X PUT -H "Content-Type: application/json" -d '{"key1":"value1", "key2":"value2"}' https://httpbin.org/put
```

```
{
  "args": {}, 
  "data": "{\"key1\":\"value1\", \"key2\":\"value2\"}", 
  "files": {}, 
  "form": {}, 
  "headers": {
    "Accept": "*/*", 
    "Content-Length": "34", 
    "Content-Type": "application/json", 
    "Host": "httpbin.org", 
    "User-Agent": "curl/7.77.0", 
    "X-Amzn-Trace-Id": "Root=1-61aa3627-343ff3647d1fd6bb4734f308"
  }, 
  "json": {
    "key1": "value1", 
    "key2": "value2"
  }, 
  "origin": "81.250.131.200", 
  "url": "https://httpbin.org/put"
}
```

#### PATCH

La méthode PATCH est utilisée pour appliquer des modifications partielles à une ressource.

#### DELETE

La méthode DELETE supprime la ressource indiquée.

```bash
curl -X DELETE https://httpbin.org/delete
```

#### CONNECT

La méthode CONNECT établit un tunnel vers le serveur identifié par la ressource cible.

#### OPTIONS

La méthode OPTIONS est utilisée pour décrire les options de communications avec la ressource visée.

#### TRACE

La méthode TRACE réalise un message de test aller/retour en suivant le chemin de la ressource visée.

### Headers

Nous avons vu que les requêtes et les réponses contiennent des en-têtes. Il existe pléthores d'en tête certaines informative, fonctionnelles, standard ou custom à un domaine.

Étudion ensemble deux en-tête fonctionnelles très utiles.

#### Authentifications (basic & digest)

Il est souvent nécessaire de limiter l'accès à un serveur. Les en-tête permettent de s'authentifier. Il existe plusieurs méthodes standard.

##### Basic Auth

Cette fois utilisons notre navigateur pour tester cette méthode d'authentification.

1. Ouvrez votre navigateur.
2. Ouvrez les outils de développement: Option + ⌘ + J (on macOS), ou Shift + CTRL + J (on Windows/Linux).
3. Choisissez l'onglet "Network" et cocher la cas "Preserve log".
4. Accédez à l'URL: `https://httpbin.org/basic-auth/matt/password`.

##### Bearer Token

Souvent les API utilisent un token pour authentifier les utilisateurs. Ce token passe dans le header `Authorization`.

`curl -X GET "https://httpbin.org/bearer" -H "accept: application/json" -H "Authorization: Bearer montoken"`

#### Cookies 🍪

Quand un client (un browser par exemple) le serveur peux lui demander d'écrire une petite donnée. On utilise souvent cette fonctionnalité pour identifier un user (cookie de session), pour tracker un utilisateur (cookie de tracking), pour stocker des données personnelles (cookie de persistance), etc.

Pour ce faire le serveru va utiliser un en-tête http spécial `Set-Cookie`. Par exemple si, en tant que serveur, je veux enregister un identifiant de session sur le navigateur de mon client je peux retourner le header `Set-Cookie: session-id=123654789`.

::: info 📝 Pour aller plus loin
Étudiez la [liste des en-têtes](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers).
:::

## Serveurs

De nombreux outils sont construit autour du protocol HTTP. Évidement il existe des clients. Nous avons vu un CLI (curl) mais il ya surtout des navigateurs. Mais il existe aussi de nombreux serveurs. Les serveur HTTP ou seveurs web peuvent être classifié en 2 grandes familles.
### Static

Les serveurs **static** hebrege des fichiers et les servent au client. Les plus connus s'appele [Apache](https://httpd.apache.org/), [Nginx](https://www.nginx.com/), [Lighttpd](https://www.lighttpd.net/)...

`docker container run --rm -p 80:80 -v ${PWD}:/var/lib/nginx/html:ro dceoy/nginx-autoindex`

### Dynamic (serveur d'application)

### Proxy

## TLS

### HTTPS

### Certificats
