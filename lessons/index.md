---
home: true
heroText: Une introduction au developpement web 👨🏻‍💻
tagline: Les fondamentaux et quelques exercices pour démarrer
actionText: C'est parti !
actionLink: /lesson1/
features:
  - title: "Leçon n°1 : Les fondamentaux"
    details: 🏗 C'est quoi HTTP ? C'est quoi un serveur web ?
  - title: "Leçon n°2 : HTML"
    details: 🤔 Markup language ? Balises ? Attributs ?
  - title: "Leçon n°3 : CSS"
    details: 🎨 Du styles en cascade ?
  - title: "Leçon n°4 : Javascript"
    details: ⚙️ Comment rendre une page interactive ?
---

<div style="text-align: center;">
  <img src="./programmer-programming.gif" alt="au boulot !" />
</div>
